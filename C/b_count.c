/* count.c 
 * compter jusqu'une limite
 */

/* j'utilse les macros du préprocesseur pour que le programme
 * fasse quelque chose à l'écran.
 */

#define DEBUG

#ifdef DEBUG
#include <stdio.h>
#endif


int counter,
  limit;

int init(){
  counter = 0;
  limit   = 10;
  return 0;
}

int endCondition(){
  #ifdef DEBUG
  printf("%d; ",counter);
  printf("Limite atteinte : %s\n" ,counter == limit ? "OUI" : "NON");
  #endif
  return counter == limit;
}

void count(){
  counter++;
}

int loop(){
  while (!endCondition()){
    count();
  }
  return 0;
}

int main(){
  if (init()){
    return 1;
  }
  loop();
  return 0;
}
