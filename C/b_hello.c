/* b_hello.c
 */

/* le langage C n'a pas d'interfaçage avec l'utilisateur definie dans
 * le langage. Pour ça, on utilise les bibliotheques.  Pour tout ce
 * qui est Input et Output on a "stdio". Pour informer le compilateur,
 * il faut le dire donc:
 */

#include <stdio.h>

/* Cela veut dire, 'inclure le fichier stdio.h à l'endroit de cette
 * instruction. C'est du "macro-remplacement" les mots #include
 * <stdio.h> sont remplacés physiquement par le contenu du fichier en
 * phase de pre-compilation.
 * toute instruction qui commence par # est exectuée par le
 * précompilateur.
 * On vera ça en grand detail plus tard"
 */

 /* $ gcc b_hello.c 
  * $ ./a.out
   Hello!$
 *
 * Ca marche masi c'est moche, sais tu corriger? (ajouter le retour
 * chariot '\n' !)
 */

int main(){
  printf("Hello");
  return  0;
}

