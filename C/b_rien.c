/* b_rien.c
 */


/* pour le compiler et executer, on comile avec 'gcc' ca veut dire Gnu
 * C Complier. C'etait ecrit par Richard Stallman, est c'est un des
 * plus grands accomplissements de l'histoire de l'informatique. On
 * l'utilise avec du respect et on prend plaisir à être parmi les
 * meilleurs...
 */

 /* $ gcc b_rien.c 
 * $ ls
 * a.out  b_rien.c
 * $ ./a.out
 * $
 * ce ne fait rien, c'est magnifique magnifique!
 */

/* un premier code C :
 * commentaires seront comme ici
 * etc etc.
 */

int main(){
  /* un premier progamme C 
   */
  return  0;
}

