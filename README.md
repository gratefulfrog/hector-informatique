# Hector Informatique

Ici on va garder tout ce qu'on fait!

On fait tout en ligne de commande Linux.

Première chose, cloner ce répo chez toi:

`$ git clone git@gitlab.com:gratefulfrog/hector-informatique.git`

Si ça ne marche pas, alors :

`$ git clone https://gitlab.com/gratefulfrog/hector-informatique.git`

Ensuite, on ne va pas faire compliqué, chacun va travailler sur ses fichiers, et ne jamais modifier les fichiers de l'autre.

On applique une convention de nommage :
- les fichiers de Bob sont només : b_quelquechose.xxx
- les fichiere de Hactor sont només : h_qqchose.yy

OK?

Puis, pour récuperer mon travail, tu dois faire un "pull"

`$ git pull`

Ca va mettre à jour ton répo localement

Puis, pour partager ton travail, avec moi, tu dois faire un "add", un "commit" et un "push" 

D'abord on ajoute tous les fichiers au repo local
`$ git add .`

puis on les commit, c'est à dire les prépare pour téléversement,
`$ git commit -a -m "ici un message qui explique le contenu ou les changements`

enfin, on push vers le repo remote.
`$ git push`
